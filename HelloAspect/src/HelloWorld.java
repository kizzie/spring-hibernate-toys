

public class HelloWorld {
    public static void say(String message) {
        System.out.println(message);
    }
    
    public static void sayToPerson(String message, String name) {
        System.out.println(name + ", " + message);
    }
    
    public static void main(String[] args) {
		HelloWorld.say("KaaaaT");
		HelloWorld.sayToPerson("oh hai", "kat");
	}
}