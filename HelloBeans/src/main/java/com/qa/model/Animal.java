package com.qa.model;

public class Animal {
	
	private int ID;
	private String name;
	private String type;
	private int age;
	
	public Animal(int ID, String name, String type, int age){
		this.setID(ID);
		this.setName(name);
		this.setType(type);
		this.setAge(age);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}
}
