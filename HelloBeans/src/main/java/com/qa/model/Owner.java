package com.qa.model;

import java.util.ArrayList;

public class Owner {

	private String name;
	private ArrayList<Animal> animals;

	public Owner(String name) {
		this.name = name;
		animals = new ArrayList<>();
	}

	public void addAnimal(Animal a) {
		animals.add(a);
	}

	public Animal getAnimal(int id) {
		for (Animal a : animals) {
			//Animal a = (Animal) a2;
			if (a.getID() == id) {
				return a;
			}
		}
		return null;
	}

	public ArrayList<Animal> getAllAnimals() {
		return animals;
	}

	public void removeAnimal(int id) {
		for (Animal a : animals) {
			//Animal a = (Animal) a2;
			if (a.getID() == id) {
				animals.remove(a);
			}
		}
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

}
