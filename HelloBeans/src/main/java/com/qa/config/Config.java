package com.qa.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.qa.model.Animal;
import com.qa.model.Owner;

//Identify as a configuration
@Configuration
// Specifies which package to scan for controllers
@ComponentScan("com.qa")
public class Config {

	// create a javabean to resolve the views as they are asked for.
	// this method tells spring that it can find all the jsp files in the
	// WEB-INF/views folder and they will have the file type of jsp.
	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	// this time we want to set up a few objects with information in them
	// these classes are held in the model
	@Bean
	public Animal angus() {
		return new Animal(0, "A", "Rabbit", 1);
	}

	@Bean
	public Animal shadow() {
		return new Animal(1, "S", "Rabbit", 1);
	}

	@Bean
	public Animal vito() {
		return new Animal(2, "V", "Cat", 2);
	}

	//we can include one bean in another by calling it using the name angus()
	@Bean
	public Owner kat() {
		Owner kat = new Owner("Kat");
		kat.addAnimal(angus());
		kat.addAnimal(shadow());
		return kat;
	}

	@Bean
	public Owner gary() {
		Owner gary = new Owner("NotKat");
		gary.addAnimal(vito());
		return gary;
	}

	//finally, a list of all the owners
	//autowired arraylists of owners in the controllers come from this definition
	@Bean
	public ArrayList<Owner> getAllOwners() {
		ArrayList<Owner> arr = new ArrayList<>();
		arr.add(kat());
		arr.add(gary());
		return arr;
	}
}
