package com.qa.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.qa.model.Owner;

//controller for the owner webpages
@Controller
public class OwnerController {
	
	//get the owners array list from the Config.java file
	@Autowired
	ArrayList<Owner> owners;
	
	//request any traffic coming in on owner to be redirected using this file
	//the name should be in the get parameters, if not then set the default value
	@RequestMapping("/owner")
	public String handleHello(@RequestParam(value = "name", required = false, defaultValue="Unknown") String name, Model model) {

		//set the owner name in the model
		model.addAttribute("OwnerName", name);
		
		boolean found = false;
		//get the specific owner object for this id. 
		for (Owner o : owners){ //for each owner
			if (o.getName().equals(name)){ //if the names match
				model.addAttribute("AnimalList", o.getAllAnimals()); //add the list of animals to the model to iterate through in the jsp
				found = true;
				break;
			}
		}

		//if we don't find the name, then set the name to unknown
		if (!found) { 
			model.addAttribute("name", "unknown");
		}
		
		//return the owner.jsp with the model
		return "owner";
	}
}


