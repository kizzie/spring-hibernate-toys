package com.qa.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qa.model.Animal;
import com.qa.model.Owner;

//define this as a controller class
@Controller
public class AnimalController {
	
	//get the owners bean generated in Config.java
	@Autowired
	ArrayList<Owner> owners;
	
	//any traffic coming in on /animal, parameters are the animal ID which is set to -1 if it is missing
	@RequestMapping("/animal")
	public String handleHello(
			@RequestParam(value = "id", required = false, defaultValue = "-1") int id, Model model) {
		
		//get the specific animal for this id.
		boolean found = false;
		for (Owner o : owners){ // for each owner
			if (found){
				break; //if we've found it, no point searching the rest
			}
			for (Animal a : o.getAllAnimals()){ //for each animal belonging to that owner
				if (a.getID() == id){ //if the animal id is the one we're looking for
					//set the model attributes for the jsp view to access 
					model.addAttribute("id", id);
					model.addAttribute("name", a.getName());
					model.addAttribute("type", a.getType());
					model.addAttribute("age", a.getAge());
					//break out the for loops as we've found the right ID. ID should be unique to each animal
					found = true;
					break;
				}
			}
		}
		
		//if we didn't find the ID given, then just set it to unknown
		if (!found) {
			model.addAttribute("id", "unknown");
		}
		
		
		//return the animal.jsp with the model information 
		return "animal";
	}
}


