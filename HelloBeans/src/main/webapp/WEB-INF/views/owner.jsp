<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
	
<!-- The tag lib from jstl lets us iterate through a collection using xml tags -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Owner Page</title>
</head>
<body>

	<!-- Get the information for the owner from the model -->
	<h1>Information for Owner: ${OwnerName}</h1>

	<h2>Animals</h2>

	<!-- Tag from the jstl library, this is the same as for (Animal a : AnimalList) -->
	<c:forEach items="${AnimalList}" var="a">
		<!-- We use the object methods to get the information. -->
		<p>		ID: <c:out value="${a.getID()}" />
		<br/>	Name: <c:out value="${a.getName()}" />
		<br/>	Type: <c:out value="${a.getType()}" />
		<br/>	Age: <c:out value="${a.getAge()}" /></p>
	</c:forEach>
</body>
</html>