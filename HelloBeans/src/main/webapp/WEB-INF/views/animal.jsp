<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Animal Details</title>
</head>
<body>

<!-- Get the information from the model passed to this page and output it -->
<h1>Information for animal ID: ${id}</h1>
<p>Name: ${name}</p>
<p>Type: ${type}</p>
<p>Age: ${age}</p>

</body>
</html>
