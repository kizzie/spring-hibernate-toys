package com.thecafetechno.main;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.thecafetechno.pojo.Warrior;

public class War {
	
	public static void main(String[] args) {
		ApplicationContext context = getContext();
		Warrior warrior = (Warrior) context.getBean("warriorBean");
		System.out.println(warrior.getWeapon());
	}

	public static ApplicationContext getContext() {
		return new ClassPathXmlApplicationContext("beans.xml");
	}
}