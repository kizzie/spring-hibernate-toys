package com.thecafetechno.pojo;

import com.thecafetechno.main.War;

public class Warrior {
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Weapon getWeapon() {
		return ((Weapon) War.getContext().getBean("gunWeaponBean"));
	}
	// public abstract Weapon getWeapon() ;
}