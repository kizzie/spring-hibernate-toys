package com.thecafetechno.pojo;

public class Weapon {
	private String name;
	private float weight;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {

		return "weapon : " + name + "\n" + "weight : " + weight + "Kg";
	}
}