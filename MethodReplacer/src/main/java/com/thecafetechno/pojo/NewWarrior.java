package com.thecafetechno.pojo;

import java.lang.reflect.Method;
import org.springframework.beans.factory.support.MethodReplacer;
import com.thecafetechno.main.War;

public class NewWarrior implements MethodReplacer {
	
	@Override
	public Object reimplement(Object target, Method method, Object[] args) throws Throwable {
		
		return ((Weapon) War.getContext().getBean("swordWeaponBean"));
		
	}
}