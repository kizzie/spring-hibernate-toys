package com.qa.HibernateAccess.ManyToManyExample;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;




@Entity
public class Employee {
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	private String name;
	
	@ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="link")
	private List<Employer> employeeList;
	
	public Employee(int id, String name, ArrayList<Employer> empList) {
		super();
		this.id = id;
		this.name = name;
		this.employeeList = empList;
	}
	
	public Employee(){}
	
	public Employee(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.employeeList = new ArrayList<Employer>();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Employer> getEmpList() {
		return employeeList;
	}

	public void setEmpList(ArrayList<Employer> empList) {
		this.employeeList = empList;
	}
	
	@Override
	public String toString(){
		String s = "ID: " + id + " Name: " + name + "\nEmployer List: ";
		for (Employer emp : employeeList){
			s += emp.getName() + ",";
		}
		return s;
	}
	
	
	
	
}
