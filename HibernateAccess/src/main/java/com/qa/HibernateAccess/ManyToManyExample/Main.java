package com.qa.HibernateAccess.ManyToManyExample;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

public class Main {

	public static void main(String[] args) {
		// setup the database

		Configuration cfg = new Configuration().configure();

		// set up the service registry
		ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
				.applySettings(cfg.getProperties()).buildServiceRegistry();

		// set up the session factory
		SessionFactory sessionFactory = cfg.addAnnotatedClass(Employee.class)
				.addAnnotatedClass(Employer.class)
				.buildSessionFactory(serviceRegistry);

		// create the objects
		Employee alice = new Employee(0, "Alice");
		Employee bob = new Employee(1, "Bob");
		Employee mallory = new Employee(2, "Mallory");
		Employee eve = new Employee(3, "Eve");

		Employer a = new Employer(0, "A");
		Employer b = new Employer(0, "B");
		Employer c = new Employer(0, "C");

		ArrayList<Employee> pair1 = new ArrayList<Employee>();
		pair1.add(alice);
		pair1.add(bob);
		pair1.add(eve);

		ArrayList<Employee> pair2 = new ArrayList<Employee>();
		pair2.add(mallory);
		pair2.add(eve);

		ArrayList<Employee> pair3 = new ArrayList<Employee>();
		pair3.add(alice);
		pair3.add(bob);
		pair3.add(mallory);
		pair3.add(eve);

		ArrayList<Employer> aliceAndBobList = new ArrayList<Employer>();
		aliceAndBobList.add(a);
		aliceAndBobList.add(c);

		ArrayList<Employer> malloryList = new ArrayList<Employer>();
		malloryList.add(b);
		malloryList.add(c);

		ArrayList<Employer> eveList = new ArrayList<Employer>();
		eveList.add(a);
		eveList.add(b);

		a.setEmpList(pair1);
		b.setEmpList(pair2);
		c.setEmpList(pair3);

		alice.setEmpList(aliceAndBobList);
		bob.setEmpList(aliceAndBobList);
		mallory.setEmpList(malloryList);
		eve.setEmpList(eveList);

		// save to the database
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		session.saveOrUpdate(a);
		session.saveOrUpdate(b);
		session.saveOrUpdate(c);
		session.saveOrUpdate(alice);
		session.saveOrUpdate(bob);
		session.saveOrUpdate(mallory);
		session.saveOrUpdate(eve);

		tx.commit();

		// get from the database

		session.close();

		session = sessionFactory.openSession();
		
		Query qry = session.createQuery("from Employee");

		ArrayList<Employee> results = new ArrayList<Employee>(qry.list());

				session.close();


		for (Employee e : results) {
			System.out.println(e);
		}
	}

}
