package com.qa.HibernateAccess.ManyToManyExample;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

/**
 * @author Kat
 *
 */
@Entity
public class Employer {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	private String name;
	
	
	 @ManyToMany(mappedBy="employeeList")
	private List<Employee> employerList;

	public Employer(int id, String name, ArrayList<Employee> empList) {
		super();
		this.id = id;
		this.name = name;
		this.employerList = empList;
	}
	
	
	public Employer(int id, String name) {
		super();
		this.id = id;
		this.name = name;
		this.employerList = new ArrayList<Employee>();
	}
	
	public Employer(){}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public List<Employee> getEmpList() {
		return employerList;
	}


	public void setEmpList(ArrayList<Employee> empList) {
		this.employerList = empList;
	}
	
	
	
}
