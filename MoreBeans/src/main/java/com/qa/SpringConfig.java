package com.qa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qa.beans.AutowiredBean;

@Configuration
public class SpringConfig {
	
	//three little beans, all in a row
	@Bean
	public AutowiredBean au(){
		return new AutowiredBean();
	}

	@Bean 
	public int anyInt() {
		return 1;
	}
	
	@Bean
	public String anyString() {
		return "Kat";
	}
}
