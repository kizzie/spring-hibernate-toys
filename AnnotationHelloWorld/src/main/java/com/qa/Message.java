package com.qa;

import org.springframework.beans.factory.annotation.Autowired;


public class Message {
	
	
	private String message;

	public Message() {
	}

	
	public Message(String msg) {
		this.message = msg;
	}

	//@Autowired
	public void setMessage(String msg) {
		this.message = msg;
	}

	public String getMessage() {
		return message;
	}
}
