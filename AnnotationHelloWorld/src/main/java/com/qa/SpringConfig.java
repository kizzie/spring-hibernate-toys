package com.qa;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qa.Message;


@Configuration
public class SpringConfig {
	
	@Bean
	public Message message1() {
		Message m = new Message();
		m.setMessage("Spring is fun.");
		return m;
	}
	
	@Bean
	public Message message2() {
		Message m = new Message();
		m.setMessage("This is a different bean!");
		return m;
	}
	
	@Bean
	public Message message3() {
		return new Message("This uses the constructor arguments");
	}
}
