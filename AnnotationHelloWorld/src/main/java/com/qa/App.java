package com.qa;

import java.util.HashMap;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.qa.Message;



public class App {
	
	public static void main(String[] args) {
		new App();
	}
	
	public App(){
		// load the application context
		
		
		//ApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringConfig.class);
		//context.register(SpringConfig.class);
	//	context.refresh();
		
		// get the bean
		System.out.println("*** getting each bean ***");
		
		Message msg1 = (Message) context.getBean("message1");
		Message msg2 = (Message) context.getBean("message2");
		Message msg3 = (Message) context.getBean("message3");

		// print out the message
		System.out.println(msg1.getMessage());
		System.out.println(msg2.getMessage());
		System.out.println(msg3.getMessage());
		
		//we can also get all the beans of one type easily
		System.out.println("*** Using a map ***");
		
		HashMap<String, Message> list = new HashMap<String, Message>(context.getBeansOfType(Message.class));
		for (String key : list.keySet()) {
			System.out.println(list.get(key).getMessage());
		}
		
	
	}
}
