<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <!-- The tag lib from jstl lets us iterate through a collection using xml tags -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>


<!-- Get the information from the model passed to this page and output it -->
<h1>Information for animal ID: ${animal.animalID}</h1>
<p>Name: ${animal.name}</p>
<p>Type: ${animal.type}</p>
<p>Date of Birth: ${animal.dob}</p>

<h2>Notes</h2>


<!-- Tag from the jstl library, this is the same as for (Animal a : AnimalList) -->
	<c:forEach items="${animal.notes}" var="note">		 
			<!-- We use the object methods to get the information. -->
			<p>ID: <c:out value="${note.getNoteID()}" /> </p>
			<ul>
			<li>	Date: <c:out value="${note.getDate()}" /> </li>
			<li>	Vet: <c:out value="${note.getVetID()}" /> </li>
			<li>	Notes: <c:out value="${note.getNotes()}" /></li>
		</ul>
		
	</c:forEach>


	
</body>
</html>