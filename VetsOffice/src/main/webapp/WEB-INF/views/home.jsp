<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>All Animals</title>
</head>
<body>
<h1>Links</h1>

<a href = "<%= request.getContextPath()%>/" >Home</a>
<a href = "<%= request.getContextPath()%>/addAnimal" >Add Animal</a>
<a href = "<%= request.getContextPath()%>/animal" >View specific animal</a>
<a href = "<%= request.getContextPath()%>/owner" >View specific owner</a>

<h1>Content</h1>
List all animals in the database... 

<c:forEach items="${animals}" var="animal">
		<h3>Animal ID: ${animal.animalID}</h3>
		<p>Name: ${animal.name}</p>
		<p>Type: ${animal.type}</p>
		<p>Date of Birth: ${animal.dob}</p>
</c:forEach>

</body>
</html>