<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!-- The tag lib from jstl lets us iterate through a collection using xml tags -->
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>



	<!-- Get the information from the model passed to this page and output it -->
<c:forEach items="${all}" var="owner">
	<h1>Information for Owner ID: ${owner.getOwnerID()}</h1>
	<p>Name: ${owner.getName()}</p>
	<p>Email: ${owner.getEmailAddress()}</p>


	<h2>Animals</h2>
	<c:forEach items="${owner.animals}" var="animal">
		<h3>Animal ID: ${animal.animalID}</h3>
		<p>Name: ${animal.name}</p>
		<p>Type: ${animal.type}</p>
		<p>Date of Birth: ${animal.dob}</p>
	</c:forEach>
</c:forEach>

</body>
</html>