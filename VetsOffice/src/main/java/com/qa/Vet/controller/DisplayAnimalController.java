package com.qa.Vet.controller;

import java.util.ArrayList;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.qa.Vet.model.Animal;

//display the animal and all its notes. 

@Controller
public class DisplayAnimalController {

	@Autowired
	ArrayList<Animal> animals;
	
//	 @Autowired
//	 SessionFactory sessionFactory;

	// any traffic coming in on /animal, parameters are the animal ID which is
	// set to -1 if it is missing
	@RequestMapping("/animal")
	public String handleHello(@RequestParam(value = "id", required = false, defaultValue = "-1") int id, Model model) {

		
		// get the specific animal for this id.

		for (Animal a : animals) { 
			if (a.getAnimalID() == id) { 
				model.addAttribute("animal", a);
				break;
			}
		}

		// return the animal.jsp with the model information
		return "animal";
	}

}
