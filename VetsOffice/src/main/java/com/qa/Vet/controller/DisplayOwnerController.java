package com.qa.Vet.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.qa.Vet.model.Owner;
import com.qa.Vet.model.dao.AnimalDAO;

//display the animal and all its notes. 

@Controller
public class DisplayOwnerController {

	@Autowired 
	AnimalDAO dao;
	
//	@Autowired
//	ArrayList<Owner> owners;

	// any traffic coming in on /animal, parameters are the animal ID which is
	// set to -1 if it is missing
	@RequestMapping("/owner")
	public ModelAndView viewOwner(@RequestParam(value = "id", required = false, defaultValue = "-1") int id) {

		if (id < 0){
			//pass all owners over from the database
			return new ModelAndView("owner", "all", dao.getAllOwners());
		} else {
			//get one owner from the database based on ID
			ArrayList<Owner> ownerList = new ArrayList<Owner>();
			ownerList.add(dao.getOwnerByID(id));
			return new ModelAndView("owner", "all", ownerList);
		}
	}

}
