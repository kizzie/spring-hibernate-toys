	package com.qa.Vet.controller;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.qa.Vet.model.Animal;
import com.qa.Vet.model.dao.AnimalDAO;

@Controller
public class DisplayAllAnimalsController {
	
	@Autowired
	private AnimalDAO dao;
	
	@RequestMapping("/")
	public ModelAndView handleRoot() {
		
		Animal a = new Animal(0, "Honey2", "Dog", new Date(System.currentTimeMillis()));
		Animal a2 = new Animal(0, "Midas3", "Cat", new Date(System.currentTimeMillis()));
////
		dao.saveAnimal(a);
		dao.saveAnimal(a2);

		ArrayList<Animal> list = dao.getAllAnimals();
		
		return new ModelAndView("home", "animals", list);

		
	}
}
