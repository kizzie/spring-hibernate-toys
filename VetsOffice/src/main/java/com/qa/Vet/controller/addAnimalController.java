package com.qa.Vet.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.qa.Vet.model.Animal;
import com.qa.Vet.model.dao.AnimalDAO;

@Controller
public class addAnimalController {

	@Autowired 
	AnimalDAO animalDAO;
	
	@RequestMapping(value = "/addAnimal", method = RequestMethod.GET)
	
	public String displayForm(){
		return "addAnimal";
	}
	
	@RequestMapping(value = "/addAnimal", method = RequestMethod.POST)
	public ModelAndView processForm(@RequestParam(value = "name") String name, @RequestParam(value = "type") String type, @RequestParam(value = "dob") String stringdob){
		Date dob;
		try {
			dob = new SimpleDateFormat("yyyy-MM-dd").parse(stringdob);
		} catch (ParseException e) {
			dob = new Date(System.currentTimeMillis());
		}
		
		boolean worked = animalDAO.saveAnimal(new Animal(0, name, type, dob));
		System.out.println("Worked = " + worked);
		return new ModelAndView("afterAddAnimal", "worked", worked);
	}
	
	
}
