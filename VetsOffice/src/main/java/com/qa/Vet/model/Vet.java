package com.qa.Vet.model;

public class Vet {
	private int vetID;
	private String name;
	
	
	public Vet(){}
	
	public Vet(int vetID, String name) {
		this.vetID = vetID;
		this.name = name;
	}
	
	public int getVetID() {
		return vetID;
	}
	
	public void setVetID(int vetID) {
		this.vetID = vetID;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	
}
