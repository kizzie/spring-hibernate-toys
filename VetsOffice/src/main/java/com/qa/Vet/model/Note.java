package com.qa.Vet.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


//@Entity
//@Table(name="Note")
public class Note {
	
//	@Id
//	@GeneratedValue(strategy=GenerationType.AUTO)
//	@Column(name="id")
	private int noteID;
	 
//	@ManyToOne
//	@JoinColumn(name="animalID")
	private int animalID;
	
//	@ManyToOne
//	@JoinColumn(name="vetID")
	private int vetID;
	
//	@Column(name="date")
	private Date date;
	
//	@Column(name="message")
	private String notes;
	
	public Note(){}
	
	public Note(int noteID, int vetID, Date date, String note) {
		this.vetID = vetID;
		this.noteID = noteID;
		this.date = date;
		this.notes = note;
	}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public int getVetID() {
		return vetID;
	}
	public void setVetID(int vetID) {
		this.vetID = vetID;
	}
	public int getNoteID() {
		return noteID;
	}
	public void setNoteID(int noteID) {
		this.noteID = noteID;
	}


}
