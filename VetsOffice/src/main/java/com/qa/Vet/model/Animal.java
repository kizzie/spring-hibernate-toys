package com.qa.Vet.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name="Animal")
public class Animal {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private int animalID;
	
//	@OneToMany(fetch = FetchType.EAGER, mappedBy = "Note")
//	private ArrayList<Note> notes;
		
//	@Column(name="name")
	private String name;
//	@Column(name="type")
	private String type;
//	@Column(name="dob")
	private Date dob;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ownerID")
	private Owner owner;
	
	public Animal() {
	}

//	public Animal(int animalID, ArrayList<Note> notes, String name,
//			String type, Date dob) {
	public Animal(int animalID, String name,
			String type, Date dob) {
		this.animalID = animalID;
		this.name = name;
		this.type = type;
		this.dob = dob;
	}

	public int getAnimalID() {
		return animalID;
	}

	public void setAnimalID(int animalID) {
		this.animalID = animalID;
	}

	
//	public ArrayList<Note> getNotes() {
//		return notes;
//	}

//	public void setNotes(ArrayList<Note> notes) {
//		this.notes = notes;
//	}

//	public void addNote(Note n) {
//		this.notes.add(n);
//	}
//
//	public Note getNote(int id) {
//		for (Note n : notes) {
//			if (n.getNoteID() == id) {
//				return n;
//			}
//		}
//
//		return null;
//	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

//	public int getOwnerID() {
//		return ownerID;
//	}
//
//	public void setOwnerID(int ownerID) {
//		this.ownerID = ownerID;
//	}
	
	

}
