package com.qa.Vet.model.dao;

import java.util.ArrayList;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.qa.Vet.model.Animal;
import com.qa.Vet.model.Owner;

public class AnimalDAO {

	@Autowired
	SessionFactory sessionFactory;

	public AnimalDAO() {
		// factory = new Configuration().configure().buildSessionFactory();
	}

	public ArrayList<Animal> getAllAnimals() {
		// SessionFactory factory = new
		// Configuration().configure().buildSessionFactory();

		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Query query = session.createQuery("FROM Animal");
		ArrayList<Animal> results = new ArrayList<Animal>(query.list());

		tx.commit();

		session.close();

		return results;
	}

	public boolean saveAnimal(Animal animal) {
		Session session = null;
		Transaction tx = null;
		try {
			session = sessionFactory.openSession();
			tx = session.beginTransaction();
			session.save(animal);
			tx.commit();

		} catch (HibernateException hbe) {
			System.out.println("Rolling back");
			tx.rollback();
			return false;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return true;

	}
	
	public Owner getOwnerByID(int ID){
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Query query = session.createQuery("FROM Owner where id = :id");
		query.setParameter("id", ID);
		Owner results = (Owner) query.list().get(0);

		tx.commit();

		session.close();

		return results;
	}
	
	public ArrayList<Owner> getAllOwners(){
		Session session = sessionFactory.openSession();

		Transaction tx = session.beginTransaction();

		Query query = session.createQuery("FROM Owner");
		ArrayList<Owner> results = new ArrayList<Owner>(query.list());

		tx.commit();

		session.close();

		return results;
	}
}
