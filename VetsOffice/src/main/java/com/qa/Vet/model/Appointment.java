package com.qa.Vet.model;

import java.util.Date;

public class Appointment {
	
	private int appointmentID;
	private Date time;
	private int ownerID;
	private int animalID;
	private int vetID;
	
	public Appointment(){}
	
	public Appointment(int ID, Date time, int owner, int animal, int vet){
		this.appointmentID = ID;
		this.time = time;
		this.ownerID = owner;
		this.animalID = animal;
		this.vetID = vet;
	}
	
	public int getAppointmentID() {
		return appointmentID;
	}
	
	public void setAppointmentID(int ID){
		this.appointmentID = ID;
	}
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	public int getOwnerID() {
		return ownerID;
	}
	public void setOwnerID(int ownerID) {
		this.ownerID = ownerID;
	}
	public int getAnimalID() {
		return animalID;
	}
	public void setAnimalID(int animalID) {
		this.animalID = animalID;
	}
	public int getVetID() {
		return vetID;
	}
	public void setVetID(int vetID) {
		this.vetID = vetID;
	}
	
}
