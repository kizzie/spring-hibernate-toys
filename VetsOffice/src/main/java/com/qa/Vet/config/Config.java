package com.qa.Vet.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.tomcat.dbcp.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.qa.Vet.model.dao.AnimalDAO;

//Identify as a configuration
@Configuration
// Specifies which package to scan for controllers
@ComponentScan("com.qa")
public class Config {

	// create a javabean to resolve the views as they are asked for.
	// this method tells spring that it can find all the jsp files in the
	// WEB-INF/views folder and they will have the file type of jsp.
	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Bean
	public AnimalDAO animalDAO() {
		return new AnimalDAO();

	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {
		
		Properties prop = new Properties();
		prop.setProperty("hibernate.hbm2ddl.auto", "update");
		prop.setProperty("hibernate.dialect","org.hibernate.dialect.MySQLDialect");
		prop.setProperty("hibernate.globally_quoted_identifiers", "true");
		
		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.qa.Vet.model" });
		sessionFactory.setHibernateProperties(prop);
		
		return sessionFactory;
	}
	
	

	@Bean
	public DataSource dataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUrl("jdbc:mysql://localhost/spring_hibernate_dev");
		dataSource.setUsername("root");
		dataSource.setPassword("root");
		
		return dataSource;
	}

}
