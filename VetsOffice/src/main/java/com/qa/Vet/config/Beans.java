package com.qa.Vet.config;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.qa.Vet.model.Animal;
import com.qa.Vet.model.Note;
import com.qa.Vet.model.Owner;
import com.qa.Vet.model.Vet;

@Configuration
public class Beans {
	
	@Bean
	public Vet becky(){
		return new Vet(0, "Becky");
	}
	
	@Bean
	public Vet peter(){
		return new Vet(1, "Peter");
	}
	
	@Bean
	public Note note1(){
		return new Note(0, 1, new Date(113,4,1), "Vaccinations done");
	}
	
	@Bean
	public Note note2(){
		return new Note(1, 1, new Date(113,4,1), "Vaccinations done");
	}
	
	@Bean
	public Note note3(){
		return new Note(2, 1, new Date(113,4,10), "Allergy to Vaccinations!");
	}
	
	@Bean
	public Note note4(){
		return new Note(3, 2, new Date(105, 7, 12), "Eye Infection");
	}
	
	@Bean
	public Note note5(){
		return new Note(4, 2, new Date(105, 7, 12), "Clean bill of health");
	}
	
	@Bean
	public Note note6(){
		return new Note(5, 2, new Date(114, 1, 2), "Vaccinations done");
	}
	
	@Bean
	public Note note7(){
		return new Note(6, 1, new Date(114, 5, 3), "Knee Replacement");
	}
	
	
	
	@Bean
	public Animal angus() {
		//Animal a = new Animal(0, new ArrayList<Note>(), "Angus", "Rabbit", new Date(113, 3, 20));
		Animal a = new Animal(0,"Angus", "Rabbit", new Date(113, 3, 20));
//		a.addNote(note1());
		
		return a;
	}
	
	@Bean
	public Animal shadow(){
		Animal a = new Animal(1, "Shadow", "Rabbit", new Date(113, 1, 1));
//		a.addNote(note2());
//		a.addNote(note3());
		return a;
	}
	
	@Bean
	public Animal bubble(){
		Animal a = new Animal(2, "Bubble", "Hamster", new Date(105, 9, 10));
//		a.addNote(note4());
		return a;
	}
	
	@Bean
	public Animal squeek() {
		Animal a = new Animal(3, "Squeek", "Hamster", new Date(105, 9, 10));
//		a.addNote(note5());
		
		return a;
	}
	
	@Bean
	public Animal honey() {
		Animal a = new Animal(4,"Honey", "Dog", new Date(108, 2, 15));
//		a.addNote(note6());
//		a.addNote(note7());
		return a;
	}
	
	@Bean
	public Owner kat() {
		Owner kat = new Owner();
		kat.setOwnerID(0);
		kat.setName("Kat");
		kat.setEmailAddress("katrina.mcivor@qa.com");
		kat.addAnimal(angus());
		kat.addAnimal(shadow());
		return kat;
	}
	
	@Bean
	public Owner sue() { 
		Owner sue = new Owner();
		sue.setOwnerID(1);
		sue.setName("Sue");
		sue.setEmailAddress("Sue@anotheremail.com");
		sue.addAnimal(honey());
		return sue;
	}
	
	@Bean
	public Owner barry() {
		Owner barry = new Owner();
		barry.setOwnerID(2);
		barry.setName("Barry");
		barry.setEmailAddress("barry@otherplace.com");
		barry.addAnimal(bubble());
		barry.addAnimal(squeek());
		return barry;
	}
	

	
	@Bean
	public ArrayList<Owner> owners() {
		ArrayList<Owner> o = new ArrayList<Owner>();
		o.add(kat());
		o.add(barry());
		o.add(sue());
		
		return o;
	}
	
	@Bean
	public ArrayList<Vet> vets() {
		ArrayList<Vet> v = new ArrayList<Vet>();
		v.add(becky());
		v.add(peter());
		return v;
	}
	
	@Bean
	public ArrayList<Animal> animals() { 
		ArrayList<Animal> a = new ArrayList<Animal>();
		a.add(angus());
		a.add(shadow());
		a.add(bubble());
		a.add(squeek());
		a.add(honey());
		
		return a;
	}
	
	@Bean
	public ArrayList<Note> notes() {
		ArrayList<Note> n = new ArrayList<Note>();
		n.add(note1());
		n.add(note2());
		n.add(note3());
		n.add(note4());
		n.add(note5());
		n.add(note6());
		n.add(note7());
		return n;
	}
	
}
