package com.qa.Vet.config;

import java.util.Date;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration.Dynamic;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import com.qa.Vet.model.Animal;
import com.qa.Vet.model.dao.AnimalDAO;

public class WebInitialiser implements WebApplicationInitializer {

	//Run when the web application is initialised and loaded to the server
	public void onStartup(ServletContext servletContext) throws ServletException {

		//specify the root context for this class
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		
		//tell the context where to find configuration classes
		rootContext.register(Config.class, Beans.class);

		//set the servlet context
		rootContext.setServletContext(servletContext);

		// Map the root directory "/" to this project, whenever any traffic comes in on localhost:8080/ the information
		// will be passed to the root context.
		Dynamic servlet = servletContext.addServlet("dispatcher", new DispatcherServlet(rootContext));
		servlet.addMapping("/");
		servlet.setLoadOnStartup(1);
	}
}
