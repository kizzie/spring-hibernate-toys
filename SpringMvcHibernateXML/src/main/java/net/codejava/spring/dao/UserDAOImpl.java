package net.codejava.spring.dao;
 
import java.util.List;

import net.codejava.spring.model.User;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
 
public class UserDAOImpl implements UserDAO {
    private SessionFactory sessionFactory;
 
    public UserDAOImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }
 
    @Override
    @Transactional
    public List<User> list() {
    	
    	Session s = sessionFactory.getCurrentSession();
    	//s.getTransaction().begin();
    	Query q = s.createQuery("update users set password = 'cookie'" + " where userid = 16");
    	int result = q.executeUpdate();
    	System.out.println("Result: " + result);
    	//s.getTransaction().commit();
    	
        @SuppressWarnings("unchecked")
        List<User> listUser = (List<User>) sessionFactory.getCurrentSession()
                .createCriteria(User.class)
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();
 
        return listUser;
    }
 
}