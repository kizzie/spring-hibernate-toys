package com.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class HelloGradleApplication {

    public static void main(String[] args) {
        SpringApplication.run(HelloGradleApplication.class, args);
    }
    
    @RequestMapping("/")
    public String hello(){
    	return "Hello world from gradle!";
    }
}
