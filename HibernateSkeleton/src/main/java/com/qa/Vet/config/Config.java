package com.qa.Vet.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate4.LocalSessionFactoryBean;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@ComponentScan("com.qa")
public class Config {

	// setup the view
	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	@Bean
	public LocalSessionFactoryBean sessionFactory() {

		Properties prop = new Properties();
		prop.setProperty("hibernate.hbm2ddl.auto", "update");
		prop.setProperty("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
		prop.setProperty("hibernate.globally_quoted_identifiers", "true");

		LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
		sessionFactory.setDataSource(dataSource());
		sessionFactory.setPackagesToScan(new String[] { "com.qa.Vet.model" });
		sessionFactory.setHibernateProperties(prop);
		return sessionFactory;
	}

	@Bean
	public DataSource dataSource() {
		BasicDataSource db = new BasicDataSource();
		db.setDriverClassName("org.hsqldb.jdbcDriver");
		db.setUrl("jdbc:hsqldb:file:c:/db/vet");
		System.out.println(db.getUrl());
		db.setUsername("SA");
		db.setPassword("");
		return db;
	}
}
