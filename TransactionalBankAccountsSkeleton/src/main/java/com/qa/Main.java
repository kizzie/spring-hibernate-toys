package com.qa;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.qa.beans.BankAccount;

public class Main {
	public static void main(String[] args) {
		// false means don't create the database
		// true means you do want to
		new Main(false);
	}

	public Main(boolean createDB) {
		// get the context
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				SpringConfig.class);

		//get a BankAccountDAO from the context
		BankAccountDAO dao = context.getBean(BankAccountDAO.class);

		if (createDB)
			dao.createBeansAndSave();

		//get the bank account objects from the database
		//**NOTE: you may need to change the ID of these accounts**
		BankAccount a = dao.getAccountByID(1);
		BankAccount b = dao.getAccountByID(2);
		
		// print out the values in the account
		System.out.println("*** BEFORE ***");
		System.out.println(a);
		System.out.println(b);

		// try to move money from one account to the other
		try {
			dao.transferFunds(a, b, 50.0);
		} catch (InsufficientBalanceException e) {
			e.printStackTrace();
		}

		// get the bank account beans from the database
		a = dao.getAccountByID(1);
		b = dao.getAccountByID(2);

		System.out.println("*** AFTER ***");
		System.out.println(a);
		System.out.println(b);
	}

}
