package com.qa;

import org.springframework.beans.factory.annotation.Required;

public class Message {
	private String message;
	
	public Message() {}
	public Message(String msg){this.message = msg;}
	
	public void setMessage(String msg){
		this.message = msg;
	}
	
	@Required
	public String getMessage() {
		return message;
	}
}
