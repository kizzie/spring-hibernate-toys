package com.qa.SpringForms.Controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.qa.SpringForms.Beans.Person;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home() {
		return "home";
	}
	
	
		
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView handleForm(	@RequestParam(value="firstname") String firstname, 
									@RequestParam(value="lastname") String lastname, 
									@RequestParam(value="age") String age, 
									@RequestParam(value="email") String email) {
		
		Person p = new Person(firstname, lastname, Integer.valueOf(age), email);
		
		return new ModelAndView("afterForm", "person", p);
	}
	
}
