package com.qa.SpringFormsXML;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import com.qa.SpringFormsXML.Beans.Person;

/**
 * Handles requests for the application home page.
 */
@Controller
@SessionAttributes("person")
public class HomeController {
	
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(ModelAndView m) {
		return "home";
	}


	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView handleForm(
			@RequestParam(value = "firstname") String firstname,
			@RequestParam(value = "lastname") String lastname,
			@RequestParam(value = "age") String age,
			@RequestParam(value = "email") String email){

		ModelAndView mv = new ModelAndView();
		Person person = new Person(firstname, lastname, Integer.valueOf(age), email);
		mv.addObject("person", person);
		mv.setViewName("afterForm");
		
		return mv;
	}

	@RequestMapping(value = "/nextPage")
	public ModelAndView handleNextPage(){//HttpServletRequest request) {
		return new ModelAndView("nextPage");
	}

}