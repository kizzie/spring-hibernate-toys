package com.qa;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qa.beans.Message;
import com.qa.beans.Owner;
import com.qa.beans.Person;

public class App {
	public static void main(String[] args) {
		new App();
	}
	
	public App(){
		//get the context from XML
		ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("spring.xml");
		
		//get the three message beans and print them out
		Message msg1 = (Message) context.getBean("msg1");
		Message msg2 = (Message) context.getBean("msg3");
		Message msg3 = (Message) context.getBean("msg2");
		
		System.out.println(msg1);
		System.out.println(msg2);
		System.out.println(msg3);
		
		//get the owner bean and print it out
		Owner ownerBean = (Owner) context.getBean("ownerBean");
		System.out.println(ownerBean);
		
		//get the person bean setup using the properties file
		Person kat = (Person) context.getBean("kat");
		System.out.println(kat);
		
		//get the context from the annotations
		AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext(SpringConfig.class);
		
		//get the three message beans
		Message msg1Annotation = (Message) context2.getBean("msg1");
		Message msg2Annotation = (Message) context2.getBean("msg3");
		Message msg3Annotation = (Message) context2.getBean("msg2");
		
		System.out.println(msg1Annotation);
		System.out.println(msg2Annotation);
		System.out.println(msg3Annotation);
		
		//get the owner bean
		Owner ownerBeanAnnotation = (Owner) context2.getBean("ownerBean");
		System.out.println(ownerBeanAnnotation);
		
		//get the person bean from the properties file
		Person katAnnotation = (Person) context2.getBean("kat");
		System.out.println(katAnnotation);
		

	}
}

