package com.qa.beans;

import java.util.ArrayList;

public class Owner {
	private String name;
	private ArrayList<Message> list;
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
	public void setList(ArrayList<Message> list){
		this.list = list;
	}
	
	public ArrayList<Message> getList() {
		return list;
	}
	
	@Override
	public String toString() {
		String s = name + "\n";
		
		for(Message m : list){
			s += m + "\n";
		}
		
		return s;
	}
	
	
}
