package com.qa.beans;

import org.springframework.beans.factory.annotation.Value;

public class Person {

	@Value( "${name}" )
	private String name;
	@Value( "${work}" )
	private String work;
	@Value( "${number_of_pets}" )
	private int numberOfPets;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getWork() {
		return work;
	}

	public void setWork(String work) {
		this.work = work;
	}

	public int getNumberOfPets() {
		return numberOfPets;
	}

	public void setNumberOfPets(int numberOfPets) {
		this.numberOfPets = numberOfPets;
	}
	
	public String toString(){
		return "Hi, my name is " + name + ". I work at " + work + ". I have " + numberOfPets + " pets at home";
	}

}
