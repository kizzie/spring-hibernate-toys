<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<title>Home</title>
</head>
<body>
	<h1>Please register your account:</h1>

	<form method="post" >
		Name: <input type="text" name="name"><br /> 
		Email: <input type="text" name="email"> <br /> 
		<input type="submit" value="Submit">
	</form>
	
	<h2>Registered Users: </h2>
		<c:forEach items="${peopleList}" var="p">
			<p>ID: ${p.getID()}
			<br />Name: ${p.getName()}
			<br />Email: ${p.getEmail()}</p>
		</c:forEach>
</body>
</html>
