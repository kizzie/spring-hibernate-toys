package com.qa.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.qa.AspectExercise.EmailExistsException;
import com.qa.beans.Person;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	@Autowired
	BasicDataSource db;

	// Base directory, gets all the people from the database
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home() throws SQLException {
		return new ModelAndView("home", "peopleList", dbGetAll());
	}

	// After the form has been posted, add a new person, then return the
	// original page with the full list
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ModelAndView process(@RequestParam(value = "name") String name,
			@RequestParam(value = "email") String email) throws SQLException,
			EmailExistsException {
		dbAdd(name, email);
		return home();
	}

	public boolean dbAdd(String name, String email)
			throws EmailExistsException, SQLException {

		boolean addedNewPerson = false;
		
		if (!personExists(email)) {
			// if no then add to database, return true
			System.out.println("Not found email");
			PreparedStatement ps2 = db.getConnection()
					.prepareStatement("insert into users values(null, ?, ?)");
			ps2.setString(1, email);
			ps2.setString(2, name);
			ps2.execute();
			addedNewPerson = true;
		}
		return addedNewPerson;
	}

	public boolean personExists(String email) throws SQLException {
		Connection conn = db.getConnection();
		PreparedStatement ps = conn.prepareStatement(
				"select * from users where email = ?",
				ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
		ps.setString(1, email);

		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			return true;
		}

		return false;
	}

	private ArrayList<Person> dbGetAll() throws SQLException {
		Connection conn = db.getConnection();
		System.out.println("got connection");
		PreparedStatement sql = conn.prepareStatement("select * from users");

		ResultSet rs = sql.executeQuery();

		ArrayList<Person> peopleList = new ArrayList<Person>();

		System.out.println("Got statement");
		while (rs.next()) {
			Person p = new Person(rs.getInt(1), rs.getString(2),
					rs.getString(3));
			peopleList.add(p);
		}

		conn.close();

		return peopleList;
	}
}
