package com.qa.AspectExercise;

public class EmailExistsException extends Exception{

	/**
	 * Exception thrown when an email exists in the database already
	 * @param email The email address in the database 
	 */
	public EmailExistsException(String email){
		super("Email Exists in DB already: " + email);
	}
	

}
