package com.qa;

import java.util.ArrayList;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import com.qa.beans.AutowiredBean;
import com.qa.beans.Database;
import com.qa.beans.Message;
import com.qa.beans.Owner;
import com.qa.beans.Publisher;

@Configuration
@PropertySource("classpath:database.properties")
public class SpringConfig {

	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}

	@Bean
	public Database db() {
		return new Database();
	}
	
	@Bean
	public Message msg1(){
		return new Message("Text 1");
	}
	
	@Bean
	public Message msg2(){
		return new Message("Text 2");
	}
	
	@Bean
	public Message msg3(){
		return new Message("Text 3");
	}
	
	@Bean
	public Message msg4(){
		return new Message("Text 4");
	}
	
	@Bean
	public String anyString(){
		return "Kat";
	}
	
	@Bean 
	public Owner ownerBean(){
		Owner o = new Owner(anyString());
		ArrayList<Message> messageList = new ArrayList<Message>();
		messageList.add(msg1());
		messageList.add(msg2());
		messageList.add(msg3());
		messageList.add(msg4());
		o.setlistOfMessages(messageList);
		return o;
	}
	
	
//	@Bean 
//	public int anyInt() {
//		return 1;
//	}
	
	//autowiring stuff
	@Bean
	public AutowiredBean au(){
		return new AutowiredBean();
	}
	
	@Bean
	public Publisher pub(){
		return Publisher.getInstance();
	}
	
	@Bean
	@Scope(BeanDefinition.SCOPE_SINGLETON)
	public Message proto(){
		return new Message("Origial");
	}
	
	@Bean 
	public AutowiredBean au2(){
		return new AutowiredBean("name1", 55);
	}
	
	@Bean
	public AutowiredBean au3(){
		AutowiredBean au3 = au2();
		au3.setName("Name2");
		return au3;
	}
	

}
