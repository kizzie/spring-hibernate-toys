package com.qa.beans;

public class Publisher {
	  private static Publisher publisher = new Publisher();
	  
	  @Override
	  public String toString(){
		  return "I am a publisher!";
	  }
	  
	  private Publisher() {
	  }
	  
	  public static Publisher getInstance() {
	    return publisher;
	  }
	}
