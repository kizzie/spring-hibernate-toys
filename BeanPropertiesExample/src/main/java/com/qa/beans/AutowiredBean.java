package com.qa.beans;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;


public class AutowiredBean implements InitializingBean, DisposableBean {
	
	@Autowired
	private String name;
	
	@Autowired (required = false)
	private int ID;
	
//	Message message;
//	
//	public void setMessage(Message message){
//		this.message = message;
//	}
	
	public AutowiredBean(){}
	
//	@Autowired
	public AutowiredBean(String name, int ID){
		this.ID = ID;
		this.name = name;
	}
	
	
	public String getName(){
		return name;
	}
	
	public int getID() {
		return ID;
	}
	
//	@Autowired
	public void setName(String name){
		System.out.println("Inside setName");
		this.name = name;
	}
//	@Autowired
	public void setID(int ID){
		System.out.println("Inside setID");
		this.ID = ID;
	}
	

//	@PostConstruct
//	public void init() throws Exception {
//		System.out.println("do setup");
//	}
//
//	@PreDestroy
//	public void close() throws Exception {
//		System.out.println("shut down ");
		
//	}

	@Override
	public void destroy() throws Exception {
		System.out.println("destroy");
		
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		System.out.println("afterprops");
		
	}
	
	@Override
	public String toString(){
		return name + ":" + ID;
	}

	
}
