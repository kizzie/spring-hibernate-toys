package com.qa.beans;



public class Message {
	
	
	private String message;

	public Message() {
	}

	
	public Message(String msg) {
		this.message = msg;
	}

	public void setMessage(String msg) {
		this.message = msg;
	}

	public String getMessage() {
		return message;
	}
	
	@Override
	public String toString(){
		return "Message: " + message;
	}
}
