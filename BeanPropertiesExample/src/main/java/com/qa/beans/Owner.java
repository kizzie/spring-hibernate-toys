package com.qa.beans;

import java.util.ArrayList;

public class Owner {
	private String name;
	
	private ArrayList<Message> listOfMessages;
	
	public Owner(String name){
		this.name = name;
		listOfMessages = new ArrayList<Message>();
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setlistOfMessages(ArrayList<Message> list){
		this.listOfMessages = list;
	}
	public ArrayList<Message> getlistOfMessages(){
		return listOfMessages;
	}
	
	@Override
	public String toString(){
		String s = name + "\n";
		
		for(Message m : listOfMessages){
			s += m + "\n";
		}
		
		return s;
		
	}
}
