package com.qa.beans.writer;

import java.lang.reflect.Method;

import org.springframework.beans.factory.support.MethodReplacer;

public class TimeOutputWriter implements MethodReplacer {
	
	public Object reimplement(Object target, Method m, Object[] args) throws Throwable {
		
		System.out.println(System.currentTimeMillis() + ": " + args[0]);
		
		return null;
	}

}
