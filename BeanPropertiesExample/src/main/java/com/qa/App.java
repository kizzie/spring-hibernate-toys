package com.qa;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.qa.beans.AutowiredBean;
import com.qa.beans.Message;
import com.qa.beans.Publisher;
import com.qa.beans.writer.OutputWriter;

public class App {
	
	public static void main(String[] args) throws Exception {
		new App();
	}
	
	public App() throws Exception{
		
		ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext("spring.xml");
		
//		AnnotationConfigApplicationContext context2 = new AnnotationConfigApplicationContext(SpringConfig.class);
//		
//		Database db = (Database) context.getBean("db");
//		Database db2 = (Database) context2.getBean("db");
//		
//		System.out.println("From XML: " + db);
//		System.out.println("From Annotation: " + db2);
//		
//		Owner o = (Owner) context.getBean("ownerBean");
//		System.out.println("******\n" + o + "******");
//		
//		Owner o2 = (Owner) context2.getBean("ownerBean");
//		System.out.println("++++++\n" + o2 + "++++++");
//	
//		Database db3 = (Database) context.getBean("myDataSource");
//		System.out.println(db3);
//		
//		Owner o3 = (Owner) context.getBean("ownerBean3");
//		System.out.println(o3);
//		
//		AutowiredBean au = (AutowiredBean) context2.getBean("au");
//		System.out.println(au.getName());
//		System.out.println(au.getID());
		
		AutowiredBean auXML = (AutowiredBean) context.getBean("au");
		System.out.println(auXML.getName());
		System.out.println(auXML.getID());
		
		
//		context2.close();
		
//		System.out.println(((Publisher) context.getBean("pub")).toString());
//		System.out.println(((Publisher) context2.getBean("pub")).toString());
//		
//		Message m1 = (Message) context2.getBean("proto");
//		System.out.println(m1.getMessage());
//		m1.setMessage	("New Message");
//		
//		Message m2 = (Message) context2.getBean("proto");
//		
//		System.out.println(m2.getMessage());
//		
////		AutowiredBean au2 = (AutowiredBean) context.getBean("au2");
////		System.out.println(au2);
//		AutowiredBean au3 = (AutowiredBean) context.getBean("au3");
//		System.out.println(au3);
//		
//		OutputWriter writer = (OutputWriter) context.getBean("outputWriter");
//		writer.write("hi");
		
	}
}
