package qa.congo;

import java.util.List;

import qa.bank.Account;
import qa.bank.BankException;

public interface BookShopDao {

	public void updateAccount(Account account) throws BankException;

	public Account findAccountByAccountNumber(
			final String accountNumber);

	public void buy(String isbn) throws ShopException;

	public Book findBookByIsbn(final String isbn);

	public List<Book> findBooksByTitleMatch(String fragment);

}