package qa.congo.client;

import java.awt.*;
import java.awt.event.*;

import qa.congo.ShopException;

@SuppressWarnings("serial")
public class CongoClient extends Frame {
	private boolean authenticated;

	public static void main(String[] args) {
		Frame frame = new CongoClient();
		frame.setLocation(200, 200);
		frame.setVisible(true);
	}
	
	public CongoClient() {
		final CongoApplication app = new CongoApplication();
		setLayout(new BorderLayout());
		Panel topPanel = new Panel();
		topPanel.setLayout(new GridLayout(2,1));
		Panel topPanel1 = new Panel();
		Label searchLabel = new Label("Search:");
		final TextField searchText = new TextField(30);
		topPanel1.add(searchLabel);
		topPanel1.add(searchText);
		topPanel.add(topPanel1);
		Panel topPanel2 = new Panel();
		Button findBookByTitle = new Button("Find Book By Title");
		topPanel2.add(findBookByTitle);
		topPanel.add(topPanel2);
		add(topPanel, BorderLayout.NORTH);
		final List resultsList = new List();
		add(resultsList, BorderLayout.CENTER);
		Panel bottomPanel = new Panel();
		bottomPanel.setLayout(new FlowLayout());
		Button addToCartButton = new Button("Add to Cart");
		Button showCartButton = new Button("Show Cart");
		bottomPanel.add(addToCartButton);
		bottomPanel.add(showCartButton);
		add(bottomPanel, BorderLayout.SOUTH);
		
		findBookByTitle.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String searchStr = searchText.getText();
				String[] results = app.findBooksByTitle(searchStr);
				resultsList.removeAll();
				for (String s : results) {
					resultsList.add(s);
				}
			}
		});

		addToCartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int index = resultsList.getSelectedIndex();
				if (index >= 0) {
					app.addToCart(index);
				}
			}
		});
		
		class CartDialog extends Dialog {
			private List cartList = new List();
			
			public CartDialog() {
				super(CongoClient.this);
				
				add(cartList, BorderLayout.CENTER);
				Panel bottomPanel = new Panel();
				bottomPanel.setLayout(new FlowLayout());
				Button removeFromCartButton = new Button("Remove From Cart");
				Button buyButton = new Button("Buy All Items");
				bottomPanel.add(removeFromCartButton);
				bottomPanel.add(buyButton);
				add(bottomPanel, BorderLayout.SOUTH);
				
				updateCartList();
				
				removeFromCartButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						int index = cartList.getSelectedIndex();
						if (index >= 0) {
							app.removeFromCart(index);
						}
						updateCartList();
					}
				});
				
				buyButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							if (!authenticated) {
								AuthDialog authDialog = new AuthDialog();
								authDialog.setLocation(500,500);
								authDialog.setVisible(true);
							}
							app.buyItemsInCart();
							updateCartList();
						} catch (ShopException se) {
							System.out.println(se.getMessage());
						}
					}					
				});
				
				addWindowListener(new WindowAdapter() {
					public void windowClosing(WindowEvent e) {
						dispose();
					}
				});
				
				setSize(300, 200);
			}
			
			private void updateCartList() {
				cartList.removeAll();
				String[] cartItems = app.listCartItems();
				for (String s : cartItems) {
					cartList.add(s);
				}
			}

			class AuthDialog extends Dialog {
				public AuthDialog() {
					super(CongoClient.this);
					Panel accountNumberPanel = new Panel();
					Label accountNumberLabel = new Label("Account Number:");
					final TextField accountNumberField = new TextField(20);
					accountNumberPanel.add(accountNumberLabel);
					accountNumberPanel.add(accountNumberField);
					Panel okPanel = new Panel();
					Button okButton = new Button("OK");
					okPanel.add(okButton);
					
					okButton.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							String accountNumber = accountNumberField.getText();
							if (accountNumber.length() > 0) {
								authenticated = true;
								app.setAccountNumber(accountNumber);
								dispose();
							}
						}	
					});
					
					add(accountNumberPanel, BorderLayout.NORTH);
					add(okPanel, BorderLayout.SOUTH);
					
					addWindowListener(new WindowAdapter() {
						public void windowClosing(WindowEvent e) {
							dispose();
						}
					});			
					setModal(true);
					setSize(350, 100);				
				}
			}
		}

		showCartButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CartDialog cartDialog = new CartDialog();
				cartDialog.setLocation(400,400);
				cartDialog.setVisible(true);
			}
		});

		
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		setSize(400, 300);
	}

}
