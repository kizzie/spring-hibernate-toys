package qa.congo.client;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import qa.congo.Book;
import qa.congo.CongoBookShop;
import qa.congo.ShopException;

public class CongoApplication {
	private List<Book> currentView = new ArrayList<Book>();
	private List<Book> cart = new ArrayList<Book>();
	private CongoBookShop shop = null;
	private String accountNumber;
	
	public CongoApplication() {
		ApplicationContext fac = new ClassPathXmlApplicationContext("bookshop.xml");
		shop = (CongoBookShop) fac.getBean("congo");
	}
	
	public String[] findBooksByTitle(String titleFragment) {
		List<Book> books = shop.findBooksByTitleMatch(titleFragment);
		String[] booksDetails = new String[books.size()];
		currentView.clear();
		for (int i=0; i<booksDetails.length; i++) {
			currentView.add(books.get(i));
			booksDetails[i] = books.get(i).getTitle();
		}
		return booksDetails;
	}

	public void addToCart(int index) {
		Book item = currentView.get(index); 
		cart.add(item);
		System.out.println(item.getTitle() + " added to cart.");
	}
	
	public String[] listCartItems() {
		String[] items = new String[cart.size()];
		for (int i=0; i<items.length; i++) {
			items[i] = cart.get(i).getTitle();
		}
		return items;
	}
	
	public void removeFromCart(int index) {
		cart.remove(index);
	}
	
	public void buyItemsInCart() throws ShopException {
		try {
			String message = shop.buy(cart, accountNumber);
			System.out.println(message);
			cart.clear();
		} catch (ShopException se) {
			throw se;
		}
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

}
