package qa.congo.client;

import java.util.ArrayList;
import java.util.List;

import qa.congo.Book;
import qa.congo.CongoBookShop;
import qa.congo.ShopException;

public class MockShop implements CongoBookShop {

	public String buy(List<Book> items, String accountNumber) throws ShopException {
		return "Buying " + items.size() + " items from account: " + accountNumber + ".";
	}

	public Book findBookByIsbn(String isbn) {
		return new Book("000000", "Dummy for Dummies", "-", 0.99);
	}

	public List<Book> findBooksByTitleMatch(String fragment) {
		List<Book> books = new ArrayList<Book>();
		if (fragment.equalsIgnoreCase("dummy")) {
			books.add(new Book("000000", "Dummy for Dummies", "-", 0.99));
			books.add(new Book("111111", "Dummy for Dummies II", "-", 0.99));
			books.add(new Book("222222", "Dummy for Dummies III", "-", 0.99));
		}
		return books;
	}


}
