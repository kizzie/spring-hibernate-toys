package qa.congo;

import java.util.ArrayList;
import java.util.Collection;

@SuppressWarnings("serial")
public class ShopException extends Exception {
	
	private Collection<ShopException> problems = new ArrayList<ShopException>();

	public ShopException() {
		super();
	}

	public ShopException(String message, Throwable cause) {
		super(message, cause);
	}

	public ShopException(String message) {
		super(message);
	}

	public ShopException(Throwable cause) {
		super(cause);
	}

	public Collection<ShopException> getProblems() {
		return problems;
	}

	public void addProblem(ShopException problem) {
		problems.add(problem);
	}
	
	public int problemCount() {
		return problems.size();
	}
	
	

}
