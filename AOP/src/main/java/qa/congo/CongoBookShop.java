package qa.congo;

import java.util.List;

public interface CongoBookShop {

	public List<Book> findBooksByTitleMatch(String fragment);
	
	public Book findBookByIsbn(String isbn);
	
	public String buy(List<Book> items, String accountNumber) throws ShopException;


}
