package qa.congo;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import qa.bank.Account;
import qa.bank.Account;
import qa.bank.BankException;

public class BookShopDaoSpring implements BookShopDao {
	private Map<String, Account> accounts;
	private List<Book> books;

	public Map<String, Account> getAccounts() {
		return accounts;
	}

	public void setAccounts(Map<String, Account> accounts) {
		this.accounts = accounts;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	public void buy(String isbn) throws ShopException {
		if (findBookByIsbn(isbn) == null) throw new ShopException("No such book!");
	}

	public Account findAccountByAccountNumber(String accountNumber) {
		return accounts.get(accountNumber);
	}

	public Book findBookByIsbn(String isbn) {
		Iterator<Book> iter = books.iterator();
		while (iter.hasNext()) {
			Book book = iter.next();
			if (book.getIsbn().equals(isbn)) {
				return book;
			}
		}
		return null;
	}

	public List<Book> findBooksByTitleMatch(String fragment) {
		List<Book> booksFound = new ArrayList<Book>();
		Iterator<Book> iter = books.iterator();
		while (iter.hasNext()) {
			Book book = iter.next();
			if (book.getTitle().toUpperCase().contains(fragment.toUpperCase())) {
				booksFound.add(book);
			}
		}
		return booksFound;
	}

	public void updateAccount(Account account) throws BankException {
		System.out.println(account.getName() + "'s account now has " + account.getBalance() + ".");
	}

}
