package qa.congo;

import java.util.List;

import qa.bank.Account;

public class CongoBookShopImpl implements CongoBookShop {
	
	private BookShopDao bookShopDao;

	public Book findBookByIsbn(String isbn) {
		return bookShopDao.findBookByIsbn(isbn);
	}

	public List<Book> findBooksByTitleMatch(String fragment) {
		return bookShopDao.findBooksByTitleMatch(fragment);
	}

	public String buy(List<Book> items, String accountNumber) throws ShopException {
		ShopException trouble = new ShopException("There were problems purchasing your items!");
		String message = "";
		double total = 0.0;
		for (Book item : items) {
			try {
				bookShopDao.buy(item.getIsbn());
				total += item.getPrice();
				message += "Successfully bought " + item.getTitle() + ".\n";
			} catch (ShopException e) {
				trouble.addProblem(e);
			}
		}
		try {
			Account fromAccount = bookShopDao.findAccountByAccountNumber(accountNumber);
			Account toAccount = bookShopDao.findAccountByAccountNumber("00099999");
			toAccount.deposit(total);
			bookShopDao.updateAccount(toAccount);
			fromAccount.withdraw(total);
			bookShopDao.updateAccount(fromAccount);
			message += "Successfully transferred " + total + " to the shop's account.\n";
		} catch (Exception e) {
			ShopException se = new ShopException(e);
			trouble.addProblem(se);
		}
		if (trouble.problemCount() > 0) throw trouble;
		return message;
	}

	public BookShopDao getBookShopDao() {
		return bookShopDao;
	}

	public void setBookShopDao(BookShopDao bookShopDao) {
		this.bookShopDao = bookShopDao;
	}
			


}
