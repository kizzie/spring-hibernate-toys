package qa.aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;


@Aspect
public class Logger {

	@Around("execution(* qa.bank.Account.*(..))")
	public Object log(ProceedingJoinPoint pre) throws Throwable {
		System.out.println("hijacked method : " + pre.getSignature().getName());
		System.out.println("hijacked arguments : " + Arrays.toString(pre.getArgs()));
		
		System.out.println("Log Before>> " + pre.getSignature().getName());
		return pre.proceed();
		
		
//		return new Object();
	}
	
//	@Pointcut("call(qa.bank.VatCollector.*(..))")
//	public void log2(ProceedingJoinPoint before) throws Throwable {
//		System.out.println("This is my log before anything in the Vat collection class: " + before.getSignature().getName());
//	}
//	
//	@Pointcut("execution(* qa.bank.VatCollector.*(..))")
//	public void log3(ProceedingJoinPoint before) throws Throwable {
//		System.out.println("This is my log after anything in the Vat collection class: " + before.getSignature().getName());
//	}

	
//	@After("execution(* qa.bank.Account.*(..))")
//	public void logAfter(ProceedingJoinPoint pre) throws Throwable {
//		System.out.println("Log After>> " + pre.getSignature().getName());
//	}
}
