package qa.bank;

import java.lang.reflect.Method;

import org.springframework.beans.factory.support.MethodReplacer;

public class VatCollector implements MethodReplacer {
	private Account vatAccount;
	private double vat;

	public Account getVatAccount() {
		return vatAccount;
	}

	public void setVatAccount(Account vatAccount) {
		this.vatAccount = vatAccount;
	}

	@Override
	public Object reimplement(Object target, Method method, Object[] params)
			throws Throwable {
		if (method.getName().equals("deposit")) {
			Account targetAccount = (Account) target;
			double amount = (Double) params[0];
			targetAccount.setBalance(targetAccount.getBalance() + amount * (1.0 - vat));
			vatAccount.deposit(amount * vat);
			System.out.println("A total of " + vatAccount.getBalance() + " VAT has been collected.");
		}
		return null;
	}

	public double getVat() {
		return vat;
	}

	public void setVat(double vat) {
		this.vat = vat;
	}

}
