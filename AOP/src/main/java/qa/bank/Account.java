package qa.bank;

public class Account {
	
	private String name;
	private String accountNumber;
	private double balance;
	
	public Account() {}
	
	public Account(String name, String accountNumber, double initialBalance) {
		this.name = name;
		this.accountNumber = accountNumber;
		this.balance = initialBalance;
	}

	public void withdraw(double amount) throws BankException {
		if (amount < 0) throw new BankException("Cannot withdraw negative amount!");
		if (balance < amount) throw new BankException("Account: " + accountNumber + " doesn't have enough money!");
		balance -= amount;
	}
	
	public void deposit(double amount) throws BankException {
		if (amount < 0) throw new BankException("Cannot deposit negative amount!");
		balance += amount;
	}
		
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getAccountNumber() {
		return accountNumber;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}

	public int hashCode() {
		if (accountNumber == null) return 0;
		return accountNumber.hashCode();
	}

	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Account other = (Account) obj;
		if (accountNumber == null) {
			if (other.accountNumber != null)
				return false;
		} else if (!accountNumber.equals(other.accountNumber))
			return false;
		return true;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	

}
