package qa.bank;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/bookshop.xml"})
public class AccountTest {
	
	@Test
	public void testAccount() {
		Account a = new Account();
		try {
			a.deposit(50);
			
			assertEquals(50.0, a.getBalance(), 0);
		} catch (BankException be){
			
		}
		
	}
}
