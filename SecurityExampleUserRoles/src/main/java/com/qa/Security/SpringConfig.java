package com.qa.Security;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@ComponentScan("com.qa.Security")
public class SpringConfig {

	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/views/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		return resolver;
	}

	 // @Bean
	 // public LocalSessionFactoryBean sessionFactory() {
	 //
	 // Properties prop = new Properties();
	 // prop.setProperty("hibernate.hbm2ddl.auto", "update");
	 //
//	 prop.setProperty("hibernate.dialect","org.hibernate.dialect.MySQLDialect");
	 // prop.setProperty("hibernate.globally_quoted_identifiers", "true");
	 //
	 // LocalSessionFactoryBean sessionFactory = new
//	 LocalSessionFactoryBean();
	 // sessionFactory.setDataSource(dataSource());
	 // sessionFactory.setPackagesToScan(new String[] { "com.qa.Vet.model" });
	 // sessionFactory.setHibernateProperties(prop);
	 // return sessionFactory;
	 // }
	 //
	 // @Bean
	 // public DataSource dataSource() {
	 // BasicDataSource dataSource = new BasicDataSource();
	 // dataSource.setDriverClassName("com.mysql.jdbc.Driver");
	 // dataSource.setUrl("jdbc:mysql://localhost/spring_hibernate_dev");
	 // dataSource.setUsername("root");
	 // dataSource.setPassword("");
	 // return dataSource;
	 // }
}
